#!/bin/sh
# vim: set sts=4 sw=4 et :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

. "${TESTDIR}/common/update-test-path"

#########
# Setup #
#########
trap "setup_failure" EXIT

TESTBINDIR=${TESTDIR}/${ARCHDIR}/bin
export TESTBINDIR

# Copy data files required by tests to bin dir.
# The tests expect the files to be present on the same dir the test is running.
# Note that linking won't work as the tests run checks to see if some of the
# data are directories, etc.
cp -rf ${TESTDIR}/data/* ${TESTBINDIR}/

# We only run the tests related to disk I/O 
GLIB_TEST_LIST="async-close-output-stream
buffered-input-stream
buffered-output-stream
converter-stream
data-input-stream
data-output-stream
file
fileattributematcher
filter-streams
g-file
g-file-info
io-stream
live-g-file
pollable
readwrite
unix-streams
vfs
"

setup_success

###########
# Execute #
###########
trap "test_failure" EXIT

for each in ${GLIB_TEST_LIST}; do
    echo "${TESTBINDIR}/$each"
done | src_test_pass

test_success
